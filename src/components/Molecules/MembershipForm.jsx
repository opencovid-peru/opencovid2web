import React, { useState, useEffect } from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import axios from "axios";
import Input from "../Atoms/Input";

const EMAIL_REGEX =
  /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

const postSubscription = ({ nombre, email }) =>
  axios.post("https://open-covid-api-vwgk4ckqbq-uk.a.run.app/api/subscribe", {
    nombre,
    email,
  });

const validateEmail = (email) => {
  let error = "";
  if (!email) {
    error = "Por favor ingrese un correo";
  } else if (!EMAIL_REGEX.test(email)) {
    error = "Por favor ingrese un correo válido";
  }
  return error;
};

const validate = (values) => {
  const errors = {};
  if (!values.firstName) {
    errors.firstName = "Por favor ingrese sus nombres";
  }

  if (!values.lastName) {
    errors.lastName = "Por favor ingrese sus apellidos";
  }

  const emailError = validateEmail(values.email);
  if (emailError) {
    errors.email = emailError;
  }

  if (!values.confirmation) {
    errors.confirmation = "Por favor, acepte los términos";
  }

  return errors;
};

const defaultValues = {
  firstName: "",
  lastName: "",
  email: "",
  confirmation: false,
};

const FullForm = ({
  initialValues = defaultValues,
  onSubmit,
  onCancel,
  submitting,
}) => {
  const [values, setValues] = useState(initialValues);
  const [errors, setErrors] = useState({});
  const [touched, setTouched] = useState({});

  const isValid = Object.keys(errors).length === 0;

  useEffect(() => {
    setValues(initialValues);
  }, [initialValues]);

  useEffect(() => {
    setErrors(validate(values));
  }, [values]);

  const handleChange = (e) =>
    setValues((currentValues) => ({
      ...currentValues,
      [e.target.name]: e.target.value,
    }));

  const handleBlur = (e) =>
    setTouched((currentTouched) => ({
      ...currentTouched,
      [e.target.name]: true,
    }));

  return (
    <form action="">
      <Form.Group controlId="first-name-input" hasValidation>
        <Form.Label>Nombres</Form.Label>
        <Form.Control
          type="text"
          name="firstName"
          className="input-personalized"
          autoComplete="none"
          value={values.firstName}
          onChange={handleChange}
          onBlur={handleBlur}
          isInvalid={errors.firstName && touched.firstName}
        />
        {errors.firstName && touched.firstName ? (
          <Form.Control.Feedback type="invalid">
            {errors.firstName}
          </Form.Control.Feedback>
        ) : null}
      </Form.Group>

      <Form.Group controlId="last-name-input" hasValidation>
        <Form.Label>Apellidos</Form.Label>
        <Form.Control
          type="text"
          name="lastName"
          className="input-personalized"
          autoComplete="none"
          value={values.lastName}
          onChange={handleChange}
          onBlur={handleBlur}
          isInvalid={errors.lastName && touched.lastName}
        />
        {errors.lastName && touched.lastName ? (
          <Form.Control.Feedback type="invalid">
            {errors.lastName}
          </Form.Control.Feedback>
        ) : null}
      </Form.Group>

      <Form.Group controlId="email-input" hasValidation>
        <Form.Label>Correo</Form.Label>
        <Form.Control
          type="email"
          name="email"
          className="input-personalized"
          value={values.email}
          onChange={handleChange}
          onBlur={handleBlur}
          placeholder="Ingrese su correo electrónico"
          isInvalid={errors.email && touched.email}
        />
        {errors.email && touched.email ? (
          <Form.Control.Feedback type="invalid">
            {errors.email}
          </Form.Control.Feedback>
        ) : null}
        <Form.Text className="text-muted">
          Opencovid-Peru se compromete a tratar su información de manera
          confidencial
        </Form.Text>
      </Form.Group>

      <Form.Group controlId="confirmation-checkbox" hasValidation>
        <Form.Check
          type="checkbox"
          label="Acepto compartir mi información"
          name="confirmation"
          value={values.confirmation}
          onChange={(e) =>
            setValues((currentValues) => ({
              ...currentValues,
              confirmation: e.target.checked,
            }))
          }
          onBlur={handleBlur}
          isInvalid={errors.confirmation && touched.confirmation}
        />
      </Form.Group>

      <div className="text-right">
        <Button variant="secondary" onClick={onCancel} className="mr-3">
          Cancelar
        </Button>
        <Button
          variant="primary"
          disabled={!isValid || submitting}
          onClick={() => {
            onSubmit(values);
          }}
        >
          {submitting ? "Enviando" : "Enviar"}
        </Button>
      </div>
    </form>
  );
};

const MembershipForm = ({ theme }) => {
  const [showModal, setShowModal] = useState(false);
  const [email, setEmail] = useState("");
  const [emailTouched, setEmailTouched] = useState(false);
  const [error, setError] = useState("");
  const [submitting, setSubmitting] = useState(false);
  const [fullFormInitialValues, setFullFormInitialValues] =
    useState(defaultValues);

  useEffect(() => {
    setError(validateEmail(email));
  }, [email]);

  const openModal = () => {
    setFullFormInitialValues({ ...defaultValues, email });
    setShowModal(true);
    setEmailTouched(false);
    setEmail("");
  };

  const submitSubscription = async ({ firstName, lastName, email }) => {
    setSubmitting(true);
    postSubscription({
      nombre: `${firstName} ${lastName}`,
      email,
    })
      .then(() => setShowModal(false))
      .catch(() => alert("Hubo un error. Por favor contacte soporte."))
      .finally(() => setSubmitting(false));
  };

  return (
    <div className={`membership-form ${theme} row`}>
      <div className="col-lg-12">
        <label
          htmlFor="membership-email"
          className="membership-form__label"
          style={{ display: "block" }}
        >
          Mantente informado, deja tu email:
        </label>
        <Input
          typeOf="email"
          idCustomed="membership-email"
          theme="membership-form__input membership-form__input--left py-2 px-3"
          placeholder="Escribe tu email"
          onBlur={() => setEmailTouched(true)}
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <Input
          typeOf="submit"
          theme="membership-form__submit membership-form__submit--right py-2 px-3"
          text="Enviar        "
          onclick={!error ? openModal : null}
        />
        {emailTouched && error ? (
          <label className="text-danger d-block">{error}</label>
        ) : null}
      </div>

      <Modal
        show={showModal}
        onHide={() => setShowModal(false)}
        centered
        backdrop="static"
      >
        <Modal.Header closeButton>
          <Modal.Title>Registro</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <FullForm
            onCancel={() => setShowModal(false)}
            onSubmit={submitSubscription}
            initialValues={fullFormInitialValues}
            submitting={submitting}
          />
        </Modal.Body>
      </Modal>
    </div>
  );
};

export default MembershipForm;
