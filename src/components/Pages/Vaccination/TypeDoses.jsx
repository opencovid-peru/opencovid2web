import React from "react";
import Table from "react-bootstrap/Table";

const TypeDoses = ({ dataTypeDoses }) => {
  const typeDose =
    dataTypeDoses == null
      ? null
      : dataTypeDoses.dosis.map((item) => (
          <tr key={item.nombre}>
            <td>{item.nombre}</td>
            <td>
              {Intl.NumberFormat().format(item.cantidad).replace(/[,.]/g, " ")}
            </td>
          </tr>
        ));

  return (
    <section className="type-doses-section">
      <Table className="table-borderless">
        <thead>
          <th>Laboratorio</th>
          <th>Total</th>
        </thead>
        <tbody>{typeDose}</tbody>
      </Table>
    </section>
  );
};

export default TypeDoses;
