import React from "react";
import SEO from '../Templates/SEO';
import metaimage from '../../img/previews/opencovid-og-image_datos históricos-02.png';

const HistoricalData = () => {
  return (
    <>
      <SEO
        title="OpenCovid-Perú - Datos Históricos"
        description="Progreso de vacunación nacional, Fallecidos reportados por el SINADEF, Movilidad de la población, Fallecidos reportados por el MINSA."
        image={metaimage}
        url="https://opencovid-peru.com/dashboard" />
      <iframe
        width="100%"
        src="https://datastudio.google.com/embed/reporting/0c5059be-0dbd-4a90-a04e-83b9a460b928/page/9xzBC"
        frameBorder="0"
        style={{ border: 0, height: "100vh" }}
        allowFullScreen
      ></iframe>
    </>
  );
};

export default HistoricalData;
